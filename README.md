# Simple Docker CI
Einfache Docker-basierte Anwendung. Das Image der Anwendung wird mitels GitLab-CI erzeugt und in die [GitLab Container-Registry](https://gitlab.com/MoeRT09/simple-docker-ci/container_registry) geladen. Anschließend wird in der CI-Pipeline ein Container mit dem image gestarte um es zu testen.

## Verwendung eines eigenen Gitlab Runners
Damit es nicht zu Problemen bei Erstellung von Docker images durch den Runner kommt, sollte dieser selbst nicht in Docker laufen. Stattdessen sollte der Runner nativ auf dem System laufen ([GitLab Dokumentation](https://docs.gitlab.com/runner/install/linux-repository.html)). Dieser kann den `docker` executor nutzen.

Nach der Registrierung des Runners im Projekt, muss die Konfiguration des Runners angepasst werden, damit dieser in der Lage ist, Docker-Images mit dem Docker-Executor zu erzeugen. Die Konfigurationsdatei ist unter folgendem Pfad zu finden: `/etc/gitlab-runner/config.toml`.

Folgende Zeilen der Konfiguration müssen angepasst werden:
```toml
[runners.docker]
    ...
    privileged = true
    ...
    volumes = ["/certs/client", "/cache"]
```

Die Zeile `privileged = true` ([informationen zum privileged mode](https://www.docker.com/blog/docker-can-now-run-within-docker/)) wird neu hinzugefügt, Das Array der Volumes wird um den Eintrag `"/certs/client"` ergänzt ([Erläuterung](https://about.gitlab.com/releases/2019/07/31/docker-in-docker-with-docker-19-dot-03/)).

Sollte der Runner bereits laufen, ließt er die geänderte Konfiguration nach dem speichern automatisch ein.