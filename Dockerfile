FROM alpine:3.11

# Install Dependencies
# RUN apk add --no-cache --virtual .fetch-deps \
#     nodejs \

WORKDIR /usr/local/bin

# Change `app.sh` to whatever your binary is called
ADD app.sh .
CMD ["./app.sh"]
